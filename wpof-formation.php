<?php
/*
 * wpof-formation.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@gabian-libre.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


function show_liste_formation( $atts )
{
    // Attributes
    $atts = shortcode_atts(
        array
            (
            'cat' => 'all',
            'tag' => 'all',
            ),
	$atts
	);
	
    $formations = get_formations();
    
    ?>
    <ul>
    <?php
    foreach($formations as $f)
    {
        ?>
        <li><a href="<?php echo get_permalink($f['ID']); ?>"><?php echo $f['titre']; ?></a></li>
        <?php
    }
    ?>
    </ul>
    <?php
    
    debug_info($formations, "formations");
}
add_shortcode( 'liste_formation', 'show_liste_formation' );


function get_formations( $atts = array(), $single = false )
{
    if (!$single)
        $formation_posts = get_posts( array( 'post_type' => 'formation', 'posts_per_page' => -1, $atts ) );
    else
        $formation_posts[0] = get_post( $atts['p'] );
    
     $formations = array();
	
    foreach ($formation_posts as $fp)
    {
	//$f = get_post_meta($fp->ID);
	$f = get_fields($fp->ID);
	$f['titre'] = $fp->post_title;
	$f['ID'] = $fp->ID;
	$f['categories'] = get_the_category($fp->ID);
	$f['tags'] = get_the_tags($fp->ID);
	
	/*
	 * Récupère l'ID de l'image. Pour afficher l'image ensuite, utiliser la fonction :
	 * wp_get_attachment_image( int $attachment_id, string|array $size = 'thumbnail', bool $icon = false, string|array $attr = '' )
	 */ 
	$f['thumbnail'] = get_post_thumbnail_id($fp->ID); 
	
	$formations[] = $f;
    }
    return $formations;
}

function get_formation_sessions($formation_id, $atts = array())
{
    $formation_session_posts = get_posts( array('post_type' => 'session_formation', 'meta_query' => array(array('key' => 'formation', 'value' => $formation_id)), 'posts_per_page' => -1, $atts));
    
    $formation_sessions = array();

    foreach ($formation_session_posts as $fs)
    {
        $f = get_fields($fs->ID);
        $f['ID'] = $fs->ID;
        
        $formation_sessions[] = $f;
    }

    if (count($formation_sessions) == 0)
        return false;
    else
        return $formation_sessions;
}


?>
