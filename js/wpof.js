jQuery(document).ready(function($)
{

// Fonction pour ouvrir ou fermer un bloc
// Le bouton doit avoir pour data-id l'id du bloc à gérer
$('.openButton').click(function(e)
{
    id = "#"+$(this).attr("data-id");
    $(id).toggleClass('blocHidden');
});

// Fonction pour ouvrir ou fermer un bloc depuis un bouton radio ou checkbox
// Le bouton doit avoir pour data-id l'id du bloc à gérer
// Pas forcément utile de perdre du temps là-dessus...
$('.openCheck').change(function(e)
{
    id = "#"+$(this).attr("data-id");
    $(id).toggleClass('blocHidden');
});

// Gestion du menu du tableau de bord de session
$('#wpof-menu ul li.bouton').click(function(e)
{
    $('#wpof-tableau .tableau').addClass('blocHidden');
    id = "#"+$(this).attr("data-id");
    $(id).toggleClass('blocHidden');
    $('#wpof-menu ul li.bouton').removeClass('highlightButton');
    $(this).addClass('highlightButton');
});

$('#wpof-menu ul li.fermer-tableau').click(function(e)
{
    id = $(this).attr("data-id");
    $('.tableau' + id).addClass('blocHidden');
    $('#wpof-menu ul li.bouton').removeClass('highlightButton');
});


// 
$('input:checkbox.has-employeur').change(function (e)
{
    id = "#" + $(this).attr("data-id");
    $('table' + id + ' .employeur-oui').toggleClass('blocHidden');
    $('table' + id + ' .employeur-non').toggleClass('blocHidden');
});


$('textarea.autoselect').click(function (e)
{
    $(this).select();
});

$('input.tarif-total-chiffre').click(function (e)
{
    id = $(this).attr("data-id");
    if ($(this).val() == "")
        $(this).val($('#nb-jour' + id).val() * $('#tarif-jour' + id).val());
});


});
