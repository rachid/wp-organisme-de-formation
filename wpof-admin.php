<?php
/*
 * wpof-admin.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@gabian-libre.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

/*
 * Définition d'un nouveau logo pour la page de login de WordPress
 */ 
function my_login_logo() { ?>
    <style type="text/css">
body.login
{
	background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png);
	background-position: center top;
	background-repeat: no-repeat;
}
</style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );
 
 
/*
 * Page(s) d'options
 */
// CSS pour l'admin
function css_admin()
{
   if (file_exists(get_stylesheet_directory_uri()."/style-admin.css"))
        $url = get_stylesheet_directory_uri()."/style-admin.css";
    else
        $url = wpof_url."style-admin.css";
    echo "<link rel='stylesheet' type='text/css' href='$url' />\n";
}
add_action('admin_head', 'css_admin');

// suppression de menus et sous-menus
function remove_submenu()
{
    global $submenu;
    //supprimer le sous menu "themes"
    //unset($submenu['themes.php'][10]);
}

function remove_menu()
{
    global $menu;

    $hidden_menus = get_option("hidden-menus");
    foreach($hidden_menus as $num => $value)
    {
        if ($value == 1) unset($menu[$num]);
    }
    //unset($menu['2']);
}
add_action('admin_head', 'remove_menu');
//add_action('admin_head', 'remove_submenu');

///////////////
// Création d'une page d'options
///////////////
add_action('admin_menu', 'wpof_admin_add_page');
function wpof_admin_add_page()
{
    add_menu_page(__('Organisme de formation'), __('Organisme de formation'), 'manage_options', 'wpof', 'wpof_options_page', 'dashicons-admin-site', 1);
//  add_submenu_page('wpof', __('Termes'), __('Termes'), 'manage_options', 'wpof_terms', 'terms_options_page');
//  add_submenu_page('wpof', 'Ajout d\'utilisateurs', 'Ajout d\'utilisateurs', 'manage_options', 'wpof_add_user', 'wpof_add_user_page');
}

//// Page principale

add_action('admin_init', 'wpof_admin_init');
function wpof_admin_init()
{
    global $wpof_options;
    if (file_exists(get_stylesheet_directory()."/terms_options.csv"))
        $csv_filename = get_stylesheet_directory()."/terms_options.csv";
    else
        $csv_filename = wpof_path."/terms_options.csv";
    
    $csv_file = fopen($csv_filename, "r");
    while (($data = fgetcsv($csv_file, 1000, "|")) !== FALSE)
    {
	if (in_array(substr($data[0], 0, 1), array("#", "h"))) continue;
	$varname = trim($data[1]);
	$wpof_options[] = $varname;
    }
    fclose($csv_file);
    
    foreach ($wpof_options as $o)
    {
	register_setting('wpof', $o);
    }
}


/*
 * Options de WPOF gérées dans l'interface d'admin (wp-admin/admin.php?page=wpof)
 * options-list est la liste des options et de leurs valeurs. Elle sert pour l'import et ne doit pas être exportée en tant qu'option.
 */
$wpof_options = array
(
    'monnaie',
    'monnaie-symbole',
    'ofadmin',
    'exotva',
    'tarif-inter',
    'hidden-menus',
    'nature-formation',
    'statut-stagiaire',
    'session-login-form',
    'financement',
    'reglement-interieur-id',
    'convention-id',
    'pdf-header',
    'pdf-footer',
    'pdf-marge-haut',
    'pdf-marge-bas',
    'pdf-marge-gauche',
    'pdf-marge-droite',
    'pdf-css',
    'options-list',
);


/*
 * Export des options du plugin au format JSON
 * Le fichier est enregistré dans le dossier du plugin
 */
function wpof_export_options()
{
    global $wpof_options;
    
    $wpof_options_values = array();
    
    foreach ($wpof_options as $key)
    {
        $wpof_options_values[$key] = get_option($key);
    }
    unset($wpof_options_values['options-list']);
    
    return json_encode($wpof_options_values, JSON_HEX_APOS|JSON_UNESCAPED_UNICODE);
}




function wpof_options_page()
{
	?>
	<div class='wrap'>
	<h2><?php echo __('Tableau de bord — Organisme de formation'); ?></h2>
	
	<?php
            // traitement de l'import éventuel
            if (get_option('options-list') != "")
            {
                $options = json_decode(get_option('options-list'), true);
                debug_info($options, "options");
                foreach ($options as $key => $value)
                {
                    update_option($key, $value, "yes");
                }
                update_option('options-list', "");
            }
	?>
	<form method="post" action="options.php">
	
	<?php
	settings_fields('wpof');
        ?>
        
        <div id="wpof-menu">
        <ul>
        <li class="bouton" id="btgeneral-options" data-id="general-options"><?php echo __("Options générales"); ?></li>
	<li class="bouton" id="btformations-options" data-id="formations-options"><?php echo __("Options des formations"); ?></li>
	<li class="bouton" id="btterms-options" data-id="terms-options"><?php echo __("Textes personnalisables"); ?></li>
	<li class="bouton" id="btpdf-options" data-id="pdf-options"><?php echo __("Options PDF"); ?></li>
	
	<?php if (current_user_can('manage_options')): ?>
	<li class="bouton" id="btwpof-admin" data-id="wpof-admin"><?php echo __("Configuration, import/export"); ?></li>
	<?php endif; ?>
	
	</ul>
	</div>
	
        <div id="wpof-tableau">
        
        <div id="general-options" class="tableau blocHidden">
	<table class="form-table">
	<tr valign="top">
		<th scope="row"><?php echo __('Organisme de formation gérant ce site'); ?></th>
		<td>
                    <?php
                        $liste = select_post_by_type("entreprise", "ofadmin", get_option('ofadmin'), __("Choisissez votre entreprise"));
                        if ($liste == NULL)
                            echo "<a href='/wp-admin/post-new.php?post_type=entreprise'>".__('Créez votre entreprise')."</a>";
                        else
                            echo $liste;
                    ?>
                </td>
	</tr>
	
	<tr valign="top">
            <th scope="row"><?php echo __("Exonération de TVA"); ?></th>
            <td><input type="checkbox" name="exotva" value="1" <?php checked(1, get_option('exotva'), true); ?> /></td>
	</tr>

	<tr valign="top">
            <th scope="row"><?php echo __("Monnaie"); ?></th>
            <td><input type="text" name="monnaie" value="<?php echo get_option('monnaie'); ?>" /></td>
            <th scope="row"><?php echo __("Symbole"); ?></th>
            <td><input type="text" name="monnaie-symbole" value="<?php echo get_option('monnaie-symbole'); ?>" /></td>
	</tr>

	<tr valign="top">
            <th scope="row"><?php echo __("Tarif inter /jour/stagiaire"); ?></th>
            <td><input type="text" name="tarif-inter" value="<?php echo get_option('tarif-inter'); ?>" /></td>
	</tr>
	
	<tr valign="top">
		<th scope="row"><?php echo __('Page du règlement intérieur'); ?></th>
		<td>
                    <?php
                        echo select_post_by_type("page", "reglement-interieur-id", get_option('reglement-interieur-id'));
                    ?>
                </td>
	</tr>
	
	<tr valign="top">
		<th scope="row"><?php echo __('Page de la convention'); ?></th>
		<td>
                    <?php
                        echo select_post_by_type("page", "convention-id", get_option('convention-id'));
                    ?>
                </td>
	</tr>
	
	</table>
	</div>
	

	<div id="formations-options" class="tableau blocHidden">
	<table class="form-table">
	
	<tr valign="top">
		<th scope="row"><?php echo __('Formulaire de connexion pour la page de session de formation'); ?></th>
		<td>
                    <?php
                        $liste = select_post_by_type("um_form", "session-login-form", get_option('session-login-form'));
                        if ($liste == NULL)
                            echo "<a href='/wp-admin/edit.php?post_type=um_form'>".__("Problème de configuration d'Ultimate Member")."</a>";
                        else
                            echo $liste;
                    ?>
                </td>
        </tr>
        
	<tr valign="top">
            <th scope="row">
                <?php echo __("Statut des stagiaires"); ?><br />
                <em><?php echo __("Un statut par ligne, chacun terminé par un retour à la ligne"); ?></em>
            </th>
        </tr>
        <tr>
            <td colspan="2">
                <textarea placeholder="<?php echo __("Un statut par ligne, chacun terminé par un retour à la ligne"); ?>" name="statut-stagiaire" cols="100" rows="8"><?php echo get_option("statut-stagiaire"); ?></textarea>
            </td>
	</tr>
	
	<tr valign="top">
            <th scope="row">
                <?php echo __("Source de financement"); ?><br />
                <em><?php echo __("Une source par ligne, chacune terminée par un retour à la ligne"); ?></em>
            </th>
        </tr>
        <tr>
            <td colspan="2">
                <textarea placeholder="<?php echo __("Une source par ligne, chacune terminée par un retour à la ligne"); ?>" name="financement" cols="100" rows="8"><?php echo get_option("financement"); ?></textarea>
            </td>
	</tr>
	
	<tr valign="top">
            <th scope="row">
                <?php echo __("Nature de l'action de formation"); ?><br />
                <em><?php echo __("Une nature par ligne, chacune terminée par un retour à la ligne"); ?></em>
            </th>
        </tr>
        <tr>
            <td colspan="2">
                <textarea placeholder="<?php echo __("Une nature par ligne, chacune terminée par un retour à la ligne"); ?>" name="nature-formation" cols="100" rows="15"><?php echo get_option("nature-formation"); ?></textarea>
            </td>
	</tr>

	</table>
	</div>
	
	<div id="terms-options" class="tableau blocHidden">
	<table class="form-table">
	<?php
        if (file_exists(get_stylesheet_directory()."/terms_options.csv"))
            $csv_filename = get_stylesheet_directory()."/terms_options.csv";
        else
            $csv_filename = wpof_path."/terms_options.csv";
	$csv_file = fopen($csv_filename, "r");
	
	while (($data = fgetcsv($csv_file, 1000, "|")) !== FALSE)
	{
		$type = trim($data[0]);
		if (substr_compare($type, "#", 0, 1) == 0) continue;
		
		// Gestion des inter-titres
		if (substr_compare($type, "h", 0, 1) == 0)
		{
                    $titre = trim($data[1]);
                    ?>
                    <tr valign="top">
			<th scope="row" colspan="2"><h3><?php echo $titre; ?></h3></th>
                    </tr>
                    
                    <?php
		}
		else
		{
		$varname = trim($data[1]);
		$label = trim($data[2]);
		?>
                    <tr valign="top">
			<th scope="row"><?php echo $label; ?></th>
			<td><input type='<?php echo $type; ?>' name='<?php echo $varname; ?>' id='<?php echo $varname; ?>' class='long-text' value="<?php echo get_option($varname); ?>" />	<p><em>Nom de l'option : <?php echo $varname; ?></em></p></td>
                    </tr>
		<?php
		}

	}
	fclose($csv_file);
	?>
	</table>
	</div>

	<div id="pdf-options" class="tableau blocHidden">
	<?php
            $editor_settings = array
            (   
                'wpautop' => false,
                'media_buttons' => true,
                'tinymce' => array
                    (   
                    'language' => 'fr',
                    'spellchecker_languages' => 'French=fr',
                    'toolbar1' => 'formatselect,bold,italic,underline,forecolor,backcolor,|,bullist,numlist,blockquote,|,justifyleft,       justifycenter,justifyright,|,link,unlink,|,spellchecker,fullscreen,wp_adv',
                    ),  
                'quicktags' => true,
                'editor_height' => 150,
            );
        ?>

	<p><?php echo __("Note : les dimensions sont en millimètres."); ?></p>
	<table class="form-table">
	<tr valign="top">
            <th scope="row"><?php echo __("En-tête"); ?></th>
            <td>
            <?php
                $content = get_option("pdf-header");
                $editor_id = 'pdf-header';
                wp_editor($content, $editor_id, $editor_settings);
            ?>
            </td>
        </tr>
	
	<tr valign="top">
            <th scope="row"><?php echo __("Pied de page"); ?></th>
            <td>
            <?php
                $content = get_option("pdf-footer");
                $editor_id = 'pdf-footer';
                wp_editor($content, $editor_id, $editor_settings);
            ?>
            </td>
        </tr>
	<tr valign="top">
            <th scope="row"><?php echo __("Logo de l'entreprise organisatrice"); ?></th>
            <td>
            <?php
                $logo = get_field("logo", get_option('ofadmin'));
                if ($logo) : ?>
                <a href="<?php echo $logo['url']; ?>"><img src="<?php echo $logo['sizes']['medium']; ?>"></a>
                <?php else :
                echo "<a href='/wp-admin/post.php?post=".get_option('ofadmin')."&action=edit'>".__("Veuillez ajouter un logo à votre entreprise !")."</a>"; ?>
                <?php endif; ?>
                    
            </td>
        </tr>
	<tr valign="top">
            <th scope="row"><?php echo __("Marges"); ?></th>
            <td>
            <label for="pdf-marge-haut"><?php echo __("haut"); ?><input type="text" id="pdf-marge-haut" name="pdf-marge-haut" value="<?php echo get_option('pdf-marge-haut'); ?>" /></label>
            <label for="pdf-marge-bas"><?php echo __("bas"); ?><input type="text" id="pdf-marge-bas" name="pdf-marge-bas" value="<?php echo get_option('pdf-marge-bas'); ?>" /></label>
            <label for="pdf-marge-gauche"><?php echo __("gauche"); ?><input type="text" id="pdf-marge-gauche" name="pdf-marge-gauche" value="<?php echo get_option('pdf-marge-gauche'); ?>" /></label>
            <label for="pdf-marge-droite"><?php echo __("droite"); ?><input type="text" id="pdf-marge-droite" name="pdf-marge-droite" value="<?php echo get_option('pdf-marge-droite'); ?>" /></label>
            </td>
	</tr>
	
	<tr valign="top">
            <th scope="row">
                <?php echo __("Feuille de style personnalisée (CSS)"); ?>
            </th>
            <td>
                <textarea name="pdf-css" cols="100" rows="20"><?php echo get_option("pdf-css"); ?></textarea>
            </td>
	</tr>
	
	
	</table>
	</div>

	<?php if (current_user_can('manage_options')): ?>
	<div id="wpof-admin" class="tableau blocHidden">
	<?php global $menu; ?>
	<table class="form-table">
	<tr valign="top">
            <th scope="row"><?php echo __("Menus cachés"); ?>
            <?php
            $hiddenable_menu = array
            (
                "2" => __("Dashboard"),
                "5" => __("Posts"),
                "10" => __("Media"),
                "20" => __("Pages"),
                "25" => __("Comments"),
                "42.78578" => "Ultimate Member",
                "75" => __("Tools"),
                "80.025" => __("ACF"),
                "100" => __("CPT UI")
            );
            ?>
            </th>
            <td>
            <?php
            $hidden_menus = get_option("hidden-menus");
            foreach ($hiddenable_menu as $hm => $menu_name)
            {
            ?>
                <p><label>
                <input type="checkbox" name="hidden-menus[<?php echo $hm; ?>]" value="1" <?php checked(1, (isset($hidden_menus["$hm"])) ? $hidden_menus["$hm"] : 0, true); ?> />
                <?php echo $menu_name; ?></label></p>
            <?php
            }
            ?>
            </td>
	</tr>	
	</table>
	<h3><?php echo __("Importer les options"); ?></h3>
	<textarea name="options-list" id="options-list" cols="80" rows="15" placeholder="<?php echo ("Collez ici les options au format JSON copiées depuis une autre instance de WPOF"); ?>"></textarea>
	
	<h3><?php echo __("Exporter les options"); ?></h3>
	<textarea class="autoselect" cols="80" rows="15" readonly><?php
        echo wpof_export_options();
	?></textarea>
	</div>
	<?php endif; ?>
	
	
	</div> <!-- wpof-tableau -->
	
	<?php
	submit_button();
	?>
        </form>
<?php
}

/*
 * Modification de la procédure d'enregistrement des sessions de formation
 * Surcharge du hook save_post
 */
function session_save_meta($post_id)
{
    if ( get_post_type($post_id) == 'session_formation' )
    {
        $session = get_fields($post_id);
        if ($session['dates'] != "")
        {
            $dates = convert_dates($session['dates']);
            $first_date = reset($dates);
            $first_timestamp = reset(array_keys($dates));
        
            $title = $session['formation']->post_title." - ".strip_tags($first_date)." - ".get_post_meta($session['lieu']->ID, 'ville', true);
        
            remove_action('save_post', 'session_save_meta', 5, 1); // Anti-infinite loop
            update_post_meta($post_id, "first_timestamp", $first_timestamp);
            wp_update_post(array('ID' => $post_id, 'post_name' => sanitize_title($title), 'post_title' => "$title"));
            add_action('save_post', 'session_save_meta', 5, 1);
        }
    }
}
add_action('save_post', 'session_save_meta', 5, 1);

