<?php
/**
 * Template part for displaying single posts.
 *
 *
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

		<div class="entry-meta">
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->
	<?php if ( has_post_thumbnail() ) : ?>
		<div class="entry-thumbnail">
			<?php the_post_thumbnail( ); ?>
		</div>
	<?php endif; ?>
	
	<?php
		$formation = get_formations(array( 'p' => get_the_ID()), true);
		$formation = $formation[0];
		$sessions = get_formation_sessions(get_the_ID());
	?>
	
	<div class="entry-content">

		<!-- Contenu court -->
		<div class="side-content">
		<?php if (isset($formation['duree'])) :?>
		<div id="formation_duree">
			<h3><?php echo __("Durée conseillée"); ?></h3>
			<p><?php printf(__("%d heures"), $formation['duree']); ?></p>
		</div>
		<?php endif; ?>
	
		<?php
		$tarif = (isset($formation['tarif_inter'])) ? $formation['tarif_inter'] : get_option('tarif-inter');
		
		if ("" != $tarif) :
		?>
		<div id="formation_tarif">
			<h3><?php echo __("Tarif"); ?></h3>
			<p><?php printf(__("%d %s"), $tarif, get_option('monnaie-symbole')); ?></p>
			<p><?php echo get_option('terms_tarif_inter').". ".get_option('terms_tarif_groupe'); ?></p>
			
		</div>
		<?php endif; ?>
	
		<?php if (isset($formation['formateur'])) :?>
		<div id="formation_formateur">
			<h3><?php echo __("Équipe pédagogique"); ?></h3>
			<ul class="list list_formateur">
			<?php
				//var_dump(get_field('formateur'));
				foreach ($formation['formateur'] as $u)
				{
					// Recherche du permalien de l'utilisateur
					$permalink_base = um_get_option('permalink_base');
					$permalink = "/user/".get_user_meta( $u['ID'], "um_user_profile_url_slug_{$permalink_base}", true );
	
					// Affichage du nom du formateur avec un lien vers son profil
					echo "<li><a href='$permalink'>".$u['display_name']."</a></li>";
				}
			?>
			</ul>
		</div>
		<?php endif; ?>
		
		<div id="formation_sessions">
		<?php
		if (false == $sessions)
                    echo "<p>".get_option('terms_no_session')."</p>";
                else
                {
                    ?><h3><?php echo __("Prochaines sessions"); ?></h3>
                    <ul class="list list_date">
                    <?php
                    foreach($sessions as $s)
                    {
                        $dates = convert_dates($s['dates']);
                        $ville = get_post_meta($s['lieu']->ID, "ville", true);
                        echo "<li><a href='".get_permalink($s['ID'])."'>".reset($dates)."</a></br>$ville</li>";
                    }
                    ?>
                    </ul>
                    <?php
                }
                
                
		
		?>
                </div>
                
		</div>


		<!-- Contenu long -->
		<div class="main-content">
	
		<?php if (isset($formation['contenus'])) :?>
		<div id="formation_contenus">
			<?php echo $formation['contenus']; ?>
		</div>
		<?php endif; ?>
	
		<div id="formation_prerequis">
			<h3><?php echo __("Pré-requis"); ?></h3>
			<?php echo $formation['pre-requis']; ?>
		</div>
	
		<div id="formation_objectifs">
			<h3><?php echo __("Objectifs"); ?></h3>
			<?php echo $formation['objectifs']; ?>
		</div>
	
		<div id="formation_programme">
			<h3><?php echo __("Programme"); ?></h3>
			<?php echo $formation['programme']; ?>
		</div>
	
		<?php //if (isset($formation[''])) :?>
		<?php //endif; ?>
	
			<?php // the_content(); /* Inutile car pas de content dans formation */ ?>
		</div> <!-- .main-content -->

		
	</div><!-- .entry-content -->

	<footer class="entry-footer">
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->

