<?php
/**
 * Template part for displaying single posts.
 *
 * 
 */

if (isset($_POST['inscription']))
    traitement_pre_inscription();
    
if (isset($_POST['admin-session-stagiaire']))
    traitement_admin_session_stagiaire();
 
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

		<div class="entry-meta">
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->
	<?php if ( has_post_thumbnail() ) : ?>
		<div class="entry-thumbnail">
			<?php the_post_thumbnail( ); ?>
		</div>
	<?php endif; ?>
	
	<?php
            $session_id = get_the_ID();
            $user_id = get_current_user_id();
            $formation = get_post_meta($session_id, "formation", true);
        ?>
	
	<div class="entry-content">
	
	
	
	<?php
            // Si le visiteur n'est pas connecté, on n'affiche que la boîte de connexion/inscription
            if ($user_id == 0):
            echo session_presentation($session_id, $formation);
            echo "<p class='important'>".get_option("terms_connexion_session")."</p>";
            $login_post = get_option("session-login-form");
            echo do_shortcode("[ultimatemember form_id=$login_post]");
            
            else: // affiche tout le reste, le endif correspondant doit se trouver tout à la fin
            
            global $ultimatemember;
            um_fetch_user($user_id);
            $role = $ultimatemember->user()->get_role();
            $f = get_fields($session_id);
            $inscrits = get_post_meta($session_id, 'inscrits');
            
	?>
	<?php if($role == 'um_stagiaire' || $role == 'administrator'): ?>
	
	<?php
	if ($role != "um_stagiaire")
	{
            echo "<p class='openButton' data-id='session_stagiaire'>Inscrire une personne</p>";
            $admin_visible = "blocHidden";
	}
	?>
	
	<div id="session_stagiaire" class="<?php echo $role;?> session_box <?php echo $admin_visible; ?>">
	<?php // Test si stagiaire inscrit à cette session
	
	if ($inscrits != null && in_array($user_id, $inscrits))
	{
            board_session_stagiaire($session_id, $user_id, $formation);
	}
	else
	{
            if ($role == 'um_stagiaire') echo session_presentation($session_id, $formation); // attention : ne pas afficher deux fois session_presentation dans une même page, pb Javascript
            echo "<p class='msg-intro msg'>".get_option("terms_msg_debut_pre_inscription")."</p>";
            echo "<form id='inscription' action='".get_permalink($session_id)."' method='post'>";
            echo hidden_input("inscription", 1);
            echo formulaire_inscription_session($session_id, $user_id);
            echo get_quiz_connaissance($session_id, $f['formation']->ID, $user_id, "eval_preform");
            echo "<p class='msg-validation msg'>".get_option("terms_msg_validation_pre_inscription")."</p>";
            echo "<input type='submit' value='".__("Validez votre pré-inscription")."'>";
            echo "</form>";
	}
	
	?>
	</div> <!-- session_box session_stagiaire -->
	
	<?php endif; ?>
	
    
	
	
	<?php if($role != 'um_stagiaire'): ?>
	<div id="session_stagiaire" class="<?php echo $role;?> session_box">
	
	<?php board_session_formateur($session_id, $user_id, $formation, $inscrits); ?>
	
	</div> <!-- session_box session_stagiaire -->
	<?php endif; ?>

		
	</div><!-- .entry-content -->
	
	<?php endif; // user_id != 0 ?>

	<footer class="entry-footer">
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->

