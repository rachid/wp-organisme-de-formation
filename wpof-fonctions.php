<?php
/*
 * wpof-fonctions.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@gabian-libre.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

/*
 * Fonctions génériques, pour tout usage
 */

/*
 * Crée une boîte radio ou cases à cocher à partir d'un texte simple (une ligne par bouton)
 * $name : nom du groupe
 * $type : radio ou checkbox
 * $value1 : valeur principale
 * $value2 : valeur secondaire
 */
function text_to_choices($text, $name, $type = 'radio', $value1 = "", $value2 = "")
{
    $html = "";
    
    $html .= "<ul>";
    
    $num = 0;
    
    global $ultimatemember;
    um_fetch_user(get_current_user_id());
    $role = $ultimatemember->user()->get_role();    
    
    foreach (preg_split("/\n/", $text) as $line)
    {
        $visible = true;
        $second_bloc = "";
        $line = explode("#", $line);
        if (isset($line[1]) && "" != $mode = trim($line[1]))
        {
            switch ($mode)
            {
                // Affichage d'un input text additionnel
                case "T":
                     $second_bloc = "<p id='${name}_".sanitize_title($line[0])."'>".trim($line[2])." <input type='text' name='${name}_".sanitize_title($line[0])."' value='".$value2."'/></p>";
                    break;
                    
                // affichage conditionné au fait que l'utilisateur ait un rôle parmi ceux cités
                case "U":
                    $auth_role = explode(",", trim($line[2]));
                    if (!in_array($role, $auth_role)) $visible = false;
                    break;

                // affichage conditionné au fait que l'utilisateur ait un rôle absent de ceux cités
                case "!U":
                case "U!":
                    $not_auth_role = explode(",", trim($line[2]));
                    if (in_array($role, $not_auth_role)) $visible = false;
                    break;
            }
        }
        
        if ($visible) $html .= "<li><label><input type='$type' name='$name' value=\"".trim($line[0])."\" ".checked($value1, trim($line[0]), false)." />$line[0]</label>$second_bloc</li>";
        $num++;
    }
    $html .= "</ul>";
    
    return $html;
}


/*
 * Crée une liste déroulante à partir d'un type de contenu
 * Chaque option aura pour identifiant l'ID du contenu et affichera le titre du contenu
 * $name : nom de la variable renseignée
 *
 */
function select_post_by_type($type, $name, $selected = "", $first = null)
{
    $posts = get_posts( array( 'post_type' => $type, 'numberposts' => -1, 'order' => 'ASC') );
    //var_dump($posts);
    $liste = array();
    if ($posts != NULL)
    {
        foreach ($posts as $p)
        {
            $liste[$p->ID] = $p->post_title;
        }
        
        $line = "<select name='".$name."'>";
	if ($first)
            $line .= "<option value='-1'>$first</option>";
            
	foreach ($liste as $id => $title)
	{
            $line .= "<option value=\"$id\" ".selected($selected, $id, false).">$title</option>";
	}
	$line .= "</select>";
	return $line;
    }
    else
        return null;
}

/*
 * Crée une liste à partir d'un tableau
 * $list : tableau de données
 * $name : nom de la liste
 * $selected : valeur sélectionnée
 */
function select_by_list($list, $name, $selected = "")
{
    $html = "<select name='$name'>";
    
    foreach ($list as $key => $value)
    {
        $html .= "<option value=\"$key\" ".selected($selected, $key, false).">$value</option>";
    }
    $html .= "</select>";
    
    return $html;
}

/*
 * Simple création d'un input hidden
 */
function hidden_input($name, $value)
{
    return "<input type='hidden' id='$name' name='$name' value=\"$value\" />";
}


/*
  Conversion de dates depuis une chaîne de caractères
  Il peut y avoir plusieurs dates
  Toutes doivent être sous le format JJ/MM/AAAA
  
  Retour : un tableau ordonné de dates avec en clé le format timestamp et en valeur quelque chose de lisible facilement
  */
function convert_dates($str)
{
    foreach(preg_split("/[\s]+/", $str) as $d)
    {
        $timestamp =  date_create_from_format("d/m/Y", $d)->getTimestamp();
        $date[$timestamp] = "<span class='jour'>".date_i18n("j", $timestamp)."</span> ";
        $date[$timestamp] .= "<span class='mois'>".date_i18n("F", $timestamp)."</span> ";
        $date[$timestamp] .= "<span class='annee'>".date_i18n("Y", $timestamp)."</span>";
    }
    
    // tri selon les indices (timestamp)
    ksort($date);
    return $date;
}


/*
 * Fonction pour afficher les dates de manière plus jolie que simplement une liste de jj/mm/aaaa
 * En entrée, du texte avec une date par ligne au format jj/mm/aaaa
 * En sortie, ce qui doit être affiché
 */
function pretty_print_dates($dates)
{
    $ts = array();
    foreach(preg_split("/[\s]+/", $dates) as $d)
    {
        $ts[] = date_create_from_format("d/m/Y", $d)->getTimestamp();
    }
    
    if (count($ts) > 1)
    {
        // Tri en ordre anti-chronologique
        rsort($ts);
        
        // On sort la première date (dernière par ordre chronologique)
        $last_ts = array_shift($ts);
        $datestr = " et ".date_i18n("j F Y", $last_ts);
    }
    else
    {
        $last_ts = 0;
        $datestr = "";
    }
    
    foreach($ts as $d)
    {
        $format = "j";
        if (date_i18n("F", $d) != date_i18n("F", $last_ts)) $format .= " F";
        if (date_i18n("Y", $d) != date_i18n("Y", $last_ts)) $format .= " Y";
        $datestr = date_i18n(", $format", $d) . $datestr;
        if (date_i18n("j", $d) == 1)
            $datestr = preg_replace("/1 /", "1<sup>er</sup> ", $datestr, 1);
        $last_ts = $d;
    }
    
    // On doit encore enlever les caractères ", " en début de chaîne
    return substr($datestr, 2);
}


function debug_info($info, $key)
{
    if (debug):

    ?>
    <h3 data-id="debug<?php echo $key;?>" class="openButton">Debug <?php echo $key; ?></h3>
    <div class="blocHidden debug" id="debug<?php echo $key;?>">
    <pre>
    <?php var_dump($info); ?>
    </pre>
    </div>
    
    <?php
    endif;
}

?>
