<?php
/*
 * wpof-session-formation.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@gabian-libre.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

require_once(wpof_path . "/wpof-pdf-tools.php");

/*
 * État d'avancement de l'inscription d'un stagiaire
 * C'est le formateur qui fait avancer cet état
 */
$etat_session = array
(
    'initial' => __("Demande d'inscription"),
    'inscrit' => __("Inscrit (convention envoyée)"),
    'confirme' => __("Confirmé (convention reçue signée)"),
    'pendant' => __("En formation"),
    'apres' => __("Formation suivie"),
    'annulok' => __("Annulation dans les délais"),
    'annulhd' => __("Annulation hors délais"),
);


function session_presentation($session_id, $formation_id)
{
    $html = "<div class='presentation'>";
	
    // Présentation de la session de formation
    $presentation = get_post_meta($session_id, "presentation", true);
    if (strip_tags($presentation) == "")
        $presentation = get_post_meta($formation_id, "contenus", true);
    $html .= wpautop($presentation);
        
    $type = get_field_object("type_formation");
    $html .= "<p>Type de session : ".$type['choices'][$type['value']]."</p>";
    $html .= "<p>Dates : ".pretty_print_dates(get_field("dates"))."</p>";
    $dates = get_field("dates");
    
    $lieu_id = get_post_meta($session_id, "lieu", true);
    $lieu = get_fields($lieu_id);
    $html .= "<p class='openButton' data-id='blocLieu'>Lieu : ".get_the_title($lieu_id)." - ".$lieu['ville']."</p>";
    $html .= "<div id='blocLieu' class='blocHidden'>";
    $html .= "<p>".$lieu['adresse']."</p>";
    $html .= $lieu['localisation'];
    $html .= "</div>";
    
    // Programme de la session de formation
    $programme = get_post_meta($session_id, "programme", true);
    if (strip_tags($programme) == "")
        $programme = get_post_meta($formation_id, "programme", true);
    
    $html .= "<p class='openButton' data-id='prog-session'>".__("Programme de la session")."</p>";
    $html .= "<div id='prog-session' class='blocHidden'>".wpautop($programme)."</div>";
        
    
    $html .= "</div> <!-- presentation -->";
    
    return $html;
}

/*
 * Affiche le formulaire d'inscription à une session de formation
 * Si rôle différent de um_stagiaire, alors possibilité d'inscrire quelqu'un d'autre
 */
function formulaire_inscription_session($session_id, $user_id)
{
    global $ultimatemember;
    um_fetch_user(get_current_user_id());
    $role = $ultimatemember->user()->get_role();
    
    // $formulaire_user_id désigne l'utilisateur concerné par le formulaire, ce qui n'est pas forcément le même si c'est un formateur qui fait l'inscription
    // mais $user_id désigne toujours l'utilisateur concerné par le formulaire, l'id du formateur est obtenu par get_current_user_id()...
    // à corriger (sans doute en supprimant $formulaire_user_id)
    //$formulaire_user_id = ($role == 'um_stagiaire') ? $user_id : 0;
    $formulaire_user_id = $user_id;
    $sd = new_session_stagiaire_data($session_id, $formulaire_user_id);
    
    // On stocke tout dans une variable texte pour afficher à la fin (ou éventuellement le retourner)
    $html = "";
       
    $html .= "<h3>".__("Identité")."</h3>";
    if ($role == "um_stagiaire")
    {
        $html .= um_user('first_name')." ".um_user('last_name');
        $html .= hidden_input("user_id", $user_id);
    }
    else
    {
        $html .= "<p>".__("Pensez à créez le stagiaire avant !")."</p>";
        $users = get_users(array('orderby' => 'display_name'));
        $html .= "<select name='user_id'>";

            foreach ($users as $u)
            {
                $html .= "<option value='".$u->ID."' ".selected($u->ID, $user_id, false).">".$u->display_name."</option>";
            }
        $html .= "</select>";
        
    }
    
    $html .= "<h3>".__("Statut")."</h3>";
    
    // On recherche s'il y a une seconde valeur (pour choix "autre")
    $valeur2 = (isset($sd['statut_'.sanitize_title($sd['statut'])])) ? $sd['statut_'.sanitize_title($sd['statut'])] : "";
    $html .= text_to_choices(get_option("statut-stagiaire"), "statut" , "radio", $sd['statut'], $valeur2);
    
    $html .= "<h3>".__("Coordonnées")."</h3>";
    
    $entreprise_objet = ($sd['entreprise'] != "") ? get_post($sd['entreprise']) : null;
    $entreprise_nom = "";
    $entreprise_adresse = "";
    $entreprise_cp_ville = "";
    $entreprise_telephone = "";
    
    //debug_info($entreprise_objet, "eo$formulaire_user_id");
    
    echo "$role";
    
    if ($role != "um_stagiaire") // On ne montre pas la liste des entreprises aux stagiaires
    {
        $html .= "<p>".__("Si l'employeur existe déjà, choisissez-le dans la liste ");
        $html .= select_post_by_type("entreprise", "entreprise", $sd["entreprise"], __("Choisissez une entreprise"));
        $html .= "</p>";
    }
    elseif ($entreprise_objet) // pour les stagiaires seulement
    {
        $html .= "<p>".__("Votre employeur : ").$entreprise_objet->post_title."</p>";
        $html .= hidden_input("entreprise", $sd['entreprise']);
    }
    if ($entreprise_objet) // pour tous les rôles
    {
        $entreprise_nom = $entreprise_objet->post_title;
        $adresse = get_post_meta($sd['entreprise'], "adresse", true);
        $cp_ville = get_post_meta($sd['entreprise'], "cp_ville", true);
        $telephone = get_post_meta($sd['entreprise'], "telephone", true);
        $entreprise_non_visible = "blocHidden";
        $entreprise_oui_visible = "";
    }
    else
    {
        $adresse = $sd['adresse'];
        $cp_ville = $sd['cp_ville'];
        $telephone = $sd['telephone'];
        $entreprise_oui_visible = "blocHidden";
        $entreprise_non_visible = "";
    }
    
    $html .= "<table id='entreprise$formulaire_user_id'>";
    $html .= "<tr><td>".get_option("terms_question_employeur")."</td><td><input type='checkbox' class='has-employeur' name='has-employeur' id='has-employeur$formulaire_user_id' data-id='entreprise$formulaire_user_id' value='1' ".checked($sd['has-employeur'], 1, false)." /></td></tr>";
    $html .= "<tr class='employeur-oui $entreprise_oui_visible'><td>".__("Nom de la structure")."</td><td><input type='text' name='entreprise-nom' id='entreprise-nom' value='".$entreprise_nom."' /></td></tr>";
    $html .= "<tr class='employeur-oui $entreprise_oui_visible'><td>".__("Service interlocuteur")."</td><td><input type='text' name='entreprise-service' id='entreprise-service' value='".$sd['entreprise-service']."' /></td></tr>";
    $html .= "<tr class='employeur-oui $entreprise_oui_visible'><td>".__("Nom du/de la responsable")."</td><td><input type='text' name='entreprise-responsable' id='entreprise-responsable' value='".$sd['entreprise-responsable']."' /></td></tr>";
    $html .= "<tr><td><span class='employeur-oui $entreprise_oui_visible'>".__("Adresse de votre employeur")."</span><span class='employeur-non $entreprise_non_visible'>".__("Votre adresse")."</span></td>";
    $html .= "<td><textarea name='adresse' id='adresse' cols='60' rows='4'>".$adresse."</textarea></td></tr>";
    
    $html .= "<tr><td>".__("Code postal et ville")."</td><td><input type='text' name='cp-ville' id='cp-ville' value='".$cp_ville."' /></td></tr>";
    $html .= "<tr><td>".__("Téléphone")."</td><td><input type='text' name='telephone' id='telephone' value='".$telephone."' /></td></tr>";
    $html .= "</table>";
    
    $html .= "<h3>".__("Financement")."</h3>";
    $html .= "<p>".__("Principale source de financement pour cette formation")."</p>";
    
    $valeur2 = (isset($sd['financement_'.sanitize_title($sd['financement'])])) ? $sd['financement_'.sanitize_title($sd['financement'])] : "";
    $html .= text_to_choices(get_option("financement"), "financement" , "radio", $sd['financement'], $valeur2);

    $html .= "<h3>".__("Nature de l'action de formation")."</h3>";

    $valeur2 = (isset($sd['nature-formation_'.sanitize_title($sd['nature-formation'])])) ? $sd['nature-formation_'.sanitize_title($sd['nature-formation'])] : "";
    $html .= text_to_choices(get_option("nature-formation"), "nature-formation" , "radio", $sd['nature-formation'], $valeur2);
    
    $html .= "<h3>".__("Vos besoins")."</h3>";
    $html .= "<p>".__("Quelles sont vos attentes par rapport à cette formation ? Comment allez-vous mettre en pratique ce que vous allez apprendre ?")."</p>";
    $html .= "<textarea cols='60' rows='15' name='attentes'>".$sd['attentes']."</textarea>";
    
    return $html;
}


/*
 * Création du questionnaire de test des connaissances (pré-requis et objectifs)
 * $form_id : ID de la formation
 * $quiz_id : indice du quiz dans la session de formation : eval_preform ou eval_postform
 * $questions : texte des questions tel que formaté dans la fiche de formation (un paragraphe = une question)
 * $form : nom du formulaire : si existant, création d'un formulaire, sinon ($form = null) on crée seulement le contenu pour intégration dans un formulaire existant
 *
 * On suppose que ces questions peuvent évoluer dans le temps (si possible pas pendant une session de formation, hein, pas de blague !) donc on va aussi stocker le texte de la question et la date de réponse
 * Les réponses à ce questionnaire ne sont stockées que dans le cadre d'une session de formation, donc pas besoin de stocker l'intitulé de la formation
 *
 */
function get_quiz_connaissance($session_id, $form_id, $user_id, $quiz_id = 'eval_preform', $questions = null, $form = null)
{
    $html = "<h2>".__("Évaluation de vos connaissances")."</h2>";
    
    if ($questions == null || $questions == "")
        $questions = get_post_meta($form_id, 'quiz', true);
    
    $sd = new_session_stagiaire_data($session_id, $user_id);
        
    $reponses = $sd[$quiz_id];

    if ($form)
        $html .= "<form id='$form' action='".get_permalink($session_id)."' method='post'>";

    $html .= "<table>";
    $num = 0;
    foreach (preg_split("/\n/", $questions) as $q)
    {
        $q = trim($q);
        if ($q == "") continue;
        
        // On suppose que les paragraphes sont les lignes qui ne commencent pas par une balise HTML (<p> est implicite dans WordPress)
        if (substr_compare($q, "<", 0, 1) != 0)
        {
            $q = trim($q);
            $html .= "<tr><td class='quiz-question'>".$q.hidden_input(sprintf("Q%02d-text", $num), $q)."</td>";
             
            $valeur = (isset($reponses[$num]['value'])) ? $reponses[$num]['value'] : "";
            $i = 0;
            while ($i < 5)
            {
                $html .= "<td class='coche'><input type='radio' name='".sprintf("Q%02d", $num)."' value='$i' ".checked($i, $valeur, false)." /></td>";
                $i++;
            }
            $html .= "</tr>\n";
            
            $num++;
        }
        else
            $html .= "<tr><td>$q</td><td>0</td><td>1</td><td>2</td><td>3</td><td>4</td></tr>\n";
        
    }
    $html .= "</table>";
    
    if ($form)
    {
        $html .= "<input type='submit' value='".__("Validez vos réponses")."'>";
        $html .= "</form>";
    }
    
    $html .= hidden_input("date", time());
    
    return $html;
}


/*
 * Traitement du formulaire de pré-inscription
 */
function traitement_pre_inscription()
{
    //debug_info($_POST, "post");
    
    $user_id = $_POST['user_id'];
    $session_id = get_the_ID();
    
    $sd = new_session_stagiaire_data($session_id, $user_id);
    $sd['statut'] = isset($_POST['statut']) ? $_POST['statut'] : "";
    
    $statut_simplifie = sanitize_title($sd['statut']);
    
    if (isset($_POST["statut_$statut_simplifie"]))
        $sd["statut_$statut_simplifie"] = $_POST["statut_$statut_simplifie"];
        
    $sd['has-employeur'] = isset($_POST['has-employeur']) ? $_POST['has-employeur'] : 0;
    if ($sd['has-employeur'] == 1)
    {
        if (isset($_POST['entreprise']) && $_POST['entreprise'] > -1)
            $sd['entreprise'] = $_POST['entreprise'];
        else
        {
            if (isset($_POST['entreprise-nom']))
            {
                $entreprise_id = wp_insert_post(array('post_title' => $_POST['entreprise-nom'], 'post_type' => 'entreprise', 'post_status' => 'publish', 'post_author' => 1), true);
                if (isset($_POST['adresse'])) update_post_meta($entreprise_id, 'adresse', $_POST['adresse']);
                if (isset($_POST['cp-ville'])) update_post_meta($entreprise_id, 'cp_ville', $_POST['cp-ville']);
                if (isset($_POST['telephone'])) update_post_meta($entreprise_id, 'telephone', $_POST['telephone']);
                $sd['entreprise'] = $entreprise_id;
            }
            else
            {
                $sd['has-employeur'] = 0; // si has-employeur est coché mais aucun nom d'entreprise donné, alors on a une incohérence, donc, on décoche de force
                $sd['adresse'] = isset($_POST['adresse']) ? $_POST['adresse'] : "";
                $sd['cp_ville'] = isset($_POST['cp-ville']) ? $_POST['cp-ville'] : "";
                $sd['telephone'] = isset($_POST['telephone']) ? $_POST['telephone'] : "";
            }
        }
    }
    else
    {
        $sd['has-employeur'] = 0; // si has-employeur est coché mais aucun nom d'entreprise donné, alors on a une incohérence, donc, on décoche de force
        $sd['adresse'] = isset($_POST['adresse']) ? $_POST['adresse'] : "";
        $sd['cp_ville'] = isset($_POST['cp-ville']) ? $_POST['cp-ville'] : "";
        $sd['telephone'] = isset($_POST['telephone']) ? $_POST['telephone'] : "";
    }
            
    $sd['entreprise-service'] = isset($_POST['entreprise-service']) ? $_POST['entreprise-service'] : "";
    $sd['entreprise-responsable'] = isset($_POST['entreprise-responsable']) ? $_POST['entreprise-responsable'] : "";
    
    $sd['financement'] = isset($_POST['financement']) ? $_POST['financement'] : "";
    
    $financement_simplifie = sanitize_title($sd['financement']);
    if (isset($_POST['financement_'.$financement_simplifie]))
        $sd['financement_'.$financement_simplifie] = $_POST['financement_'.$financement_simplifie];
    
    $sd['nature-formation'] = isset($_POST['nature-formation']) ? $_POST['nature-formation'] : "";
    
    $sd['attentes'] = isset($_POST['attentes']) ? $_POST['attentes'] : "";
    
    $i = 0;
    $num = "Q00";
    $eval_preform = array();
    
    while (isset($_POST[$num."-text"]))
    {
        $eval_preform[$i]['text'] = $_POST[$num."-text"];
        if (isset($_POST[$num])) $eval_preform[$i]['value'] = $_POST[$num];
        $i++;
        $num = sprintf("Q%02d", $i);
    }
    $sd['eval_preform'] = $eval_preform;
    
    $sd['date'] = $_POST['date'];

    update_user_meta($user_id, 'session'.$session_id, json_encode($sd, JSON_HEX_APOS|JSON_UNESCAPED_UNICODE));
    
    $inscrits = get_post_meta($session_id, 'inscrits');
    if (!in_array($user_id, $inscrits))
        add_post_meta($session_id, 'inscrits', $user_id);

//    debug_info($sd, "sd");

}

/*
 * Fonction de Traitement des données saisies par le formateur dans l'onglet admin du tableau de bord stagiaire
 */
function traitement_admin_session_stagiaire()
{
    $data = array();
    $data['etat-session'] = $_POST['etat-session'];
    $data['tarif-jour'] = $_POST['tarif-jour'];
    $data['tarif-total-chiffre'] = $_POST['tarif-total-chiffre'];
    $data['tarif-total-lettre'] = $_POST['tarif-total-lettre'];
    $data['nb-jour'] = $_POST['nb-jour'];
    $data['nb-heure'] = $_POST['nb-heure'];

    //debug_info($data, "session data");
    update_session_data($_POST['session_id'], $_POST['user_id'], $data);
}


/*
 * Initialise une structure session_stagiaire
 */
function new_session_stagiaire_data($session_id, $user_id)
{
    $sd = array
    (
	'etat-session' => 'initial',
	'attentes' => "",
	'date' => "",
	'has-employeur' => 0,
	'entreprise' => "",
	'entreprise-responsable' => "",
	'entreprise-service' => "",
	'adresse' => "",
	'cp_ville' => "",
	'telephone' => "",
	'eval_preform' => "",
	'eval_postform' => "",
	'financement' => "",
	'nature-formation' => "",
	'statut' => "",
	'etat-session' => "",
	'tarif-jour' => "",
	'tarif-total-chiffre' => "",
	'tarif-total-lettre' => "",
	'nb-jour' => "",
	'nb-heure' => "",
    );
    
    $session = json_decode(get_user_meta($user_id, "session".$session_id, true), true);
    
    if ($session)
        return array_merge($sd, $session);
    else
        return $sd;
}


/*
 * Mise à jour de valeur(s) d'une session stagiaire (session_data)
 */
function update_session_data($session_id, $user_id, $data = array())
{
    $sd = json_decode(get_user_meta($user_id, "session".$session_id, true), true);
    $update = array_merge($sd, $data);
    update_user_meta($user_id, 'session'.$session_id, json_encode($update, JSON_HEX_APOS|JSON_UNESCAPED_UNICODE));
}


/*
 * Tableau de bord du stagiaire pour une session donnée
 * $session_id : ID de la session
 * $user_id : ID du stagiaire
 * $formation_id : ID de la formation
 */
function board_session_stagiaire($session_id, $user_id, $formation_id)
{
    // création d'un id unique pour les éléments : utile lorsque plusieurs sont affichés sur la même page (vue formateur)
    $uid = "$session_id-$user_id";
    
    // récupération du rôle
    global $ultimatemember;
    um_fetch_user(get_current_user_id());
    $role = $ultimatemember->user()->get_role();
    $session = get_post($session_id);
    
    // session data
    $sd = json_decode(get_user_meta($user_id, "session".$session_id, true), true);
    
    pdf_convention($session_id, $user_id, $formation_id);
    
    global $etat_session;
    
    ?>
    
    <div id="wpof-menu" class="<?php echo $sd['etat-session']; ?>">
    <ul>
    <?php if ($role != 'um_stagiaire'): ?>
    <li data-id="<?php echo $uid; ?>" id="btwpof-tableau<?php echo $uid; ?>" class="fermer-tableau dashicons-before dashicons-arrow-up-alt2"></li>
    <?php endif; ?>
    
    <li data-id="presentation<?php echo $uid; ?>" id="btpresentation<?php echo $uid; ?>" class="bouton"><?php echo __("Présentation"); ?></li>
    
    <?php if ($role != 'um_stagiaire' || $sd['etat-session'] == 'initial'): ?>
    <li data-id="preinscription<?php echo $uid; ?>" id="btpreinscription<?php echo $uid; ?>" class="bouton"><?php echo __("Dossier de pré-inscription"); ?></li>
    <?php endif; ?>
    
    <li data-id="documents<?php echo $uid; ?>" id="btdocuments<?php echo $uid; ?>" class="bouton"><?php echo __("Documents administratifs"); ?></li>
    <li data-id="eval_postform<?php echo $uid; ?>" id="bteval_postform<?php echo $uid; ?>" class="bouton"><?php echo __("Évaluation des connaissances"); ?></li>
    
    <?php if ($role != 'um_stagiaire'): ?>
    <li data-id="admin<?php echo $uid; ?>" id="btadmin<?php echo $uid; ?>" class="bouton"><?php echo __("Admin"); ?></li>
    <?php endif; ?>
    </ul>
    
    </div>
    
    <div id="wpof-tableau">
        <div id="presentation<?php echo $uid; ?>" class="tableau tableau<?php echo $uid; ?> blocHidden">
            <?php echo session_presentation($session_id, $formation_id); ?>
        </div>
        
    <?php if ($role != 'um_stagiaire' || $sd['etat-session'] == 'initial'): ?>
        <div id="preinscription<?php echo $uid; ?>" class="tableau tableau<?php echo $uid; ?> blocHidden">
        <?php
            echo "<form id='inscription' action='".get_permalink($session_id)."' method='post'>";
            echo hidden_input("inscription", 1);
            echo formulaire_inscription_session($session_id, $user_id);
            echo get_quiz_connaissance($session_id, $formation_id, $user_id, "eval_preform");
            echo "<p class='msg-validation msg'>".get_option("terms_msg_validation_pre_inscription")."</p>";
            echo "<input type='submit' value='".__("Validez votre pré-inscription")."'>";
            echo "</form>";
        ?>
        </div>
    <?php endif; ?>
    
        <div id="documents<?php echo $uid; ?>" class="tableau tableau<?php echo $uid; ?> blocHidden">
            <?php echo "Documents administratifs !!!!!"; ?>
        </div>
        
    <?php if ($role != 'um_stagiaire' || in_array($sd['etat-session'], array('pendant', 'apres'))): ?>
        <div id="eval_postform<?php echo $uid; ?>" class="tableau tableau<?php echo $uid; ?> blocHidden">
            <?php echo get_quiz_connaissance($session_id, $formation_id, $user_id, 'eval_postform', null, "quiz_postform"); ?>
        </div>
    <?php endif; ?>
        
        <?php if ($role != 'um_stagiaire'): ?>
        
        <div id="admin<?php echo $uid; ?>" class="tableau tableau<?php echo $uid; ?> blocHidden">
        <form id="admin-session-stagiaire<?php echo $uid; ?>" action="<?php get_permalink($session_id) ?>" method='post'>
        <input type="hidden" id="admin-session-stagiaire<?php echo $uid; ?>" name="admin-session-stagiaire" value="1" />
        <input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id; ?>" />
        <input type="hidden" id="session_id" name="session_id" value="<?php echo $session_id; ?>" />
        
        <p><?php echo __("Changer l'état d'inscription"); ?>
        <?php echo select_by_list($etat_session, "etat-session", $sd['etat-session']); ?>
        </p>
        
        <p><?php echo __("Tarif journalier"); ?>
        <input type="number" name="tarif-jour" id="tarif-jour<?php echo $uid; ?>" value="<?php echo $sd['tarif-jour']; ?>" /> <?php echo get_option('monnaie-symbole'); 
        $tarif_base = (get_field("tarif", $session_id) != "") ? get_field("tarif", $session_id) : get_option("tarif-inter");
        echo " (".__("tarif de base annoncé ")."$tarif_base ".get_option('monnaie-symbole').")"; ?>
        </p>
                
        <p><?php echo __("Durée en jours"); ?>
        <input type="number" name="nb-jour" id="nb-jour<?php echo $uid; ?>" value="<?php echo $sd['nb-jour']; ?>" />
        <?php
        $duree_base = count(convert_dates(get_field("dates", $session_id)));
        echo " (".__("durée annoncée ")."$duree_base jour";
        echo ($duree_base > 1) ? "s" : "";
        echo ")"; ?>
        </p>
        
        <p><?php echo __("Durée en heures"); ?>
        <input type="number" name="nb-heure" id="nb-heure<?php echo $uid; ?>" value="<?php echo $sd['nb-heure']; ?>" /></p>
        
        <p><?php echo __("Tarif total en chiffres"); ?>
        <input type="number" name="tarif-total-chiffre" class="tarif-total-chiffre" id="tarif-total-chiffre<?php echo $uid; ?>" data-id="<?php echo $uid; ?>" value="<?php echo $sd['tarif-total-chiffre']; ?>" /></p>
        
        <p><?php echo __("Tarif total en lettres"); ?>
        <input type="text" name="tarif-total-lettre" id="tarif-total-lettre<?php echo $uid; ?>" value="<?php echo $sd['tarif-total-lettre']; ?>" size="40" /> <?php echo get_option('monnaie')."s"; ?></p>
        
        <input type="submit" name="valider" value="<?php echo __('Valider'); ?>" />
        </form>
        
	<?php if (debug): ?>
        <p>J'ai le rôle <?php echo $role ?></p>
        <ul>
            <li>Stagiaire (ID) : <?php echo get_user_meta($user_id, "first_name", true)." ".get_user_meta($user_id, "last_name", true)." ($user_id)"; ?></li>
        </ul>
        <?php debug_info($session, "session"); ?>
        <?php endif; ?>
        
        </div>
        <?php endif; // fin if role != um_stagiaire ?>
    </div>
    
    <?php
}



/*
 * Tableau de bord du formateur pour une session donnée
 * $session_id : ID de la session
 * $user_id : ID du stagiaire
 * $formation_id : ID de la formation
 * $inscrits : tableau contenant les ID des stagiaires inscrits
 */
function board_session_formateur($session_id, $user_id, $formation_id, $inscrits)
{
    // debug_info($inscrits, "inscrits");
    
    foreach ($inscrits as $i)
    {
        echo "<div class='board-nom-stagiaire'>".get_user_meta($i, "first_name", true)." ".get_user_meta($i, "last_name", true)."</div>";
        board_session_stagiaire($session_id, $i, $formation_id);
    }
}
