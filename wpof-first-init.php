<?php
/*
 * wpof-first-init.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@gabian-libre.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

/*
 * Initialisation du plugin lors de l'installation
 */
/*
 * Ne fonctionne pas.
 * Raison indéterminée :(
 * Pas important, à résoudre plus tard ou jamais
 *  
// Page des formations
$formation_page_arr = array
(
	'post_type' => 'page',
	'post_status' => 'publish',
	'post_title' => __('Formations', 'wpof'),
	'post_content' => '[formation_list]',
	'post_author' => 1,
);

wp_insert_post($formation_page_arr, true);


// Page des formateurs
$formateur_page_arr = array
(
	'post_type' => 'page',
	'post_status' => 'publish',
	'post_title' => __('Formateurs', 'wpof'),
	'post_content' => '[formateur_list]',
	'post_author' => 1,
);

wp_insert_post($formateur_page_arr, true);
*/
?>

