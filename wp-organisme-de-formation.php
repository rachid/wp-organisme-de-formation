<?php
/**
 * Plugin Name: Organisme de formation
 * Plugin URI: http://formations.artefacts.coop
 * Description: Plugin permettant de gérer l'administration de la formation professionnelle selon la loi française. Gestion des inscriptions, création automatique des documents obligatoires, suivi des stagiaires
 * Version: 0.1
 * Author: Dimitri Robert – CAE Artefacts
 * Author URI:  http://formation-logiciel-libre.com
 * License: GPL3 license
 * Depends: Ultimate Member, Custom Post Type UI, Advanced Custom Fields, WP-Pro-Quiz
 */

    error_reporting(E_ALL);
    ini_set('display_errors','On');

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

define( 'WPOF_VERSION', '0.1' );
define( 'WPOF_WP_VERSION', get_bloginfo( 'version' ) );
define( 'wpof_url', plugin_dir_url(__FILE__ ));
define( 'wpof_path', plugin_dir_path(__FILE__ ));
define( 'debug', true );

setlocale(LC_ALL, 'fr_FR@utf8');

add_action( 'wp_enqueue_scripts', 'wpof_load_scripts' );
add_action( 'admin_enqueue_scripts', 'wpof_load_scripts' );
function wpof_load_scripts()
{
    wp_enqueue_script( 'jquery' );
    wp_enqueue_script( 'wpof', wpof_url."js/wpof.js", array('jquery') );
    wp_enqueue_style( 'wpof', wpof_url."css/wpof.css" );
}


if ( is_admin())
{
	require_once(wpof_path . "/wpof-admin.php");
}

require_once(wpof_path . "/wpof-fonctions.php");
require_once(wpof_path . "/wpof-first-init.php");
require_once(wpof_path . "/wpof-session-formation.php");
require_once(wpof_path . "/wpof-formation.php");

?>

